from django.contrib import admin
from . import models


admin.site.site_header = 'Courses Admin'
admin.site.site_title = 'My Courses'
admin.site.index_title = 'Welcome to the courses admin area'


class CourseInline(admin.TabularInline):
    model = models.Course
    exclude = ['created_at']
    extra = 1


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'created_at')
    inlines = [CourseInline]


class CourseAdmin(admin.ModelAdmin):
    list_display = ('title', 'price', 'category')


admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.Course, CourseAdmin)
