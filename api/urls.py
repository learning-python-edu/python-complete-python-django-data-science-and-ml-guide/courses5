from django.urls import include, path
from tastypie.api import Api

from api.models import CategoryResource, CourseResource

api = Api(api_name='v1')
category_resource = CategoryResource()
course_resource = CourseResource()
api.register(category_resource)
api.register(course_resource)

urlpatterns = [
    path('', include(api.urls)),
]
